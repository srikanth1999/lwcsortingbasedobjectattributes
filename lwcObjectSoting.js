import { LightningElement, track, api, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class LwcObjectSoting extends LightningElement {

  @track list1 = [];
  @track list2 = [];

  connectedCallback() {
    this.getSort();
    
  }

  dynamicsort(property, order) {
    var sort_order = 1;
    if (order === "desc") {
      sort_order = -1;
    }
    return function (a, b) {
      // a should come before b in the sorted order
      if (a[property] < b[property]) {
        return -1 * sort_order;
        // a should come after b in the sorted order
      } else if (a[property] > b[property]) {
        return 1 * sort_order;
        // a and b are the same
      } else {
        return 0 * sort_order;
      }
    }
  }
  getSort() {
    var cart = [{ item: "Berry", qty: 2 },
    { item: "Apple", qty: 1 },
    { item: "Kiwi", qty: 3 },
    { item: "Kywi", qty: 6 }];
    console.log("Object to be sorted");
    console.log(cart);
    console.log("Sorting based on the item property")
    console.log(JSON.stringify(cart.sort(this.dynamicsort("item", "desc"))));
    console.log("Sorting based on the qty property")
    console.log(JSON.stringify(cart.sort(this.dynamicsort("qty", "asc"))));
    this.list1 = cart;
    this.list2 = cart.sort(this.dynamicsort("item", "desc"));
  }

}